Sequel.migration do
  up do
    create_table :users do
      primary_key :id
      String :email, null: false
      String :token
      TrueClass :is_verified
    end
    add_index :users, :email
  end

  down do
    drop_index :users, :email
    drop_table :users
  end
end
