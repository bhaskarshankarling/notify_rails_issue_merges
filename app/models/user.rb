class User < Sequel::Model
  def subscribe_to_newsletter
    self.token = Digest::MD5.hexdigest("#{email}#{Time.now.to_i}")
    self.save
    SubscriptionMailer.verification_mail(self).deliver
  end

  def genuine?(so_called_token)
    so_called_token == token
  end

  def send_newsletter
    client = Octokit::Client.new(:access_token => GITHUB_ACCESS_TOKEN)
    @merges = client.search_issues 'repo:rails/rails base:master type:pr is:merged'
    @merges = @merges[:items]
    self.set(token: nil, is_verified: true)
    self.save
    SubscriptionMailer.subscription_mail(email, @merges).deliver
  end

  def verified?
    is_verified
  end

  class << self
    def fetch_or_create(email)
      user = User.where(email: email).first
      return user unless user.nil?
      User.new(email: email)
    end
  end

  private

  def validate
    super
    errors.add(:email, "Email canot be blank") if email.nil?
    errors.add(:email, "Enter a proper email id") unless email =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  end
end
