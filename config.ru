$: << Dir.pwd
require 'web/application'
Web::Application.init
require 'sinatra/reloader' if Web::Application.environment == 'development'

run Sinatra::Application
