module Web
  module Application
    module DatabaseUtils
      attr_accessor :db_connection

      def orm(options)
        if options.keys.first == :sqlite
          db_config = options[:sqlite]
          @db_connection = Sequel.connect("sqlite://#{db_config[:path]}")
          # Sequel.extension :migration
          # begin
          #   Sequel::Migrator.check_current(db_connection, "./db/migrate/")
          # rescue Sequel::Migrator::NotCurrentError
          #   puts "Running necessary migrations at first"
          #   Sequel::Migrator.run(db_connection, "./db/migrate/")
          # end
          return
        end
        raise "Set valid orm and config"
      end
    end
  end
end
