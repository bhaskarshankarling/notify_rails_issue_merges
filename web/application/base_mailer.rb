module Web
  module Application
    class BaseMailer
      def haml(tempalte)
        Haml::Engine.new(template_content(tempalte)).render(binding)
      end

      def template_content(tempalte)
        template_path = "#{self.class.mailer_path}/#{tempalte}.haml"
        if Pathname(template_path).exist?
          File.read(template_path)
        else
          raise "Invalid template name"
        end
      end

      def mail(options)
        @mail_options = options
      end

      def deliver
        Pony.mail(@mail_options.merge(BaseMailer.default_mailer_options[:pony]))
      end

      def host
        BaseMailer.default_mailer_options[:host]
      end

      class << self
        def set_default_mailer_options(options)
          @default_mailer_options = options
        end

        def default_mailer_options
          @default_mailer_options
        end

        def mailer_path(path=nil)
          return @root_mailer_path if path.nil?
          if Pathname(path).exist?
            @root_mailer_path = path
          else
            raise "Invalid mailer path"
          end
        end

        def mailer(mailer)
          @mailers ||= []
          @mailers << mailer
        end

        def method_missing(method, *args, &block)
          if @mailers.include?(method)
            mailer_instance = self.new
            mailer_instance.send(method, *args)
            mailer_instance
          else
            super(method, *args, &block)
          end
        end
      end
    end
  end
end
