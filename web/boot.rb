require 'rubygems'

# Set up gems listed in the Gemfile.
ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../../Gemfile', __FILE__)
ENV['PORT'] = Hash[[ARGV]]["-p"] unless ARGV.empty?

require 'bundler/setup' if File.exists?(ENV['BUNDLE_GEMFILE'])
Bundler.require if defined?(Bundler)
require "sinatra/json"
