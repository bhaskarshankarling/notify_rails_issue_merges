require "letter_opener" if Web::Application.environment == :development
Web::Application.configure(:development) do
  set :root, File.expand_path(File.join(File.dirname(__FILE__), ".."))
  set :public_folder, proc { File.join(root, 'app/assets') }
  set :views, proc { File.join(root, 'app/views') }
  set :haml, { layout: :'layouts/main' }
  set :orm, sqlite: { path: 'db/data.db' }
  set :mailer_options, {
    pony: {
      via: LetterOpener::DeliveryMethod,
      via_options: { location: File.expand_path('../../tmp/letter_opener', __FILE__) }
    },
    host: "http://#{settings.bind}:#{settings.port}"
  }
end
