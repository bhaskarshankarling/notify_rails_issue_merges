# notify_rails_issue_merges

A ruby application for notifying subscribers about merged issues to rails repo. Follow below instructions to start the app. This application built using Ruby(2.5.0), Sinatra(2.0.3) web framework and Puma server. Please make sure that Ruby 2.5.0 is installed before running the application.

```bash
> git clone -b development https://gitlab.com/bhasky93/notify_rails_issue_merges.git
> bundle install
> rake db:migrate
> puma
```

Use below command to access the application console
```bash
app-root-folder> ./bin/console
```
